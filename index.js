
let num1, num2, operation, result;

do {
    num1 = prompt("Enter your number 1");
    num2 = prompt("Enter your number 2");
} while (isNaN(+num1) ||
         num1 == "" ||
         isNaN(+num2) ||
         num2 == ""   )

operation = prompt("Enter operation +, -, *, /");

function counter() {

    switch (operation) {
        case "+" :
            result = (+num1 + +num2);
            console.log(result);
            break;
        case "-" :
            result = num1 - num2;
            console.log(result);
            break;
        case "*" :
            result = num1 * num2;
            console.log(result);
            break;
        case "/" :
            result = num1 / num2;
            console.log(result);
            break;

        default:
            break;
    }
}
counter(num1, num2, operation);

